package com.skillbranch.skillbranch.utils;

/**
 * Interface that contains constants of this app
 *
 * @author Sergey Vorobyev
 */

public interface Constants {

    String LOG_TAG_PREFIX = "Skill";

    String HOUSE_LANNISTER_ID = "229";
    String HOUSE_STARK_ID = "362";
    String HOUSE_TARGARYEN_ID = "378";

}
