package com.skillbranch.skillbranch.utils;

import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.skillbranch.skillbranch.App;
import com.skillbranch.skillbranch.R;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import retrofit2.adapter.rxjava.HttpException;

/**
 * @author Sergey Vorobyev
 */

@SuppressWarnings("unused")
public class UIUtils {

    public static void showToast(Handler handler, String message) {
        if (handler != null) {
            handler.post(() -> showToast(message));
        } else {
            showToast(message);
        }
    }

    public static void showToast(String message) {
        Toast.makeText(App.get(), message, Toast.LENGTH_LONG).show();
    }

    public static void handleResponseError(Handler handler,
                                           String logTag, Throwable error) {
        if (error instanceof HttpException) {
            HttpException exception = (HttpException) error;
            if (exception.code() == 403) {
                Log.d(logTag, "handleError(): wrong username or password");
                showToast(handler, App.get().getString(R.string.error_wrong_username_or_password));
            } else {
                Log.d(logTag, "handleError(): response code is " + exception.code());
                showToast(handler, App.get().getString(R.string.error_unknown_error)
                        + " " + exception.code());
            }
        } else if (error instanceof SocketTimeoutException || error instanceof UnknownHostException) {
            Log.e(logTag, "onResponse(): check internet connection", error);
            showToast(handler, App.get().getString(R.string.error_failed_to_connect_to_server));
        } else if (error instanceof Exception) {
            Log.e(logTag, "onResponse(): unknown error", error);
            showToast(handler, App.get().getString(R.string.error_unknown_error));
        }
    }
}
