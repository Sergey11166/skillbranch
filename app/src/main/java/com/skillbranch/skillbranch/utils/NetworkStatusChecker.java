package com.skillbranch.skillbranch.utils;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.skillbranch.skillbranch.App;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * @author Sergey Vorobyev.
 */

public class NetworkStatusChecker {

    public static boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) App.get().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnectedOrConnecting();
    }
}
