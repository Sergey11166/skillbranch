package com.skillbranch.skillbranch.ui.views;

import com.hannesdorfmann.mosby.mvp.lce.MvpLceView;
import com.skillbranch.skillbranch.data.db.entities.MemberEntity;

import java.util.List;

/**
 * @author Sergey Vorobyev
 */

public interface MembersPageView extends MvpLceView<List<MemberEntity>> {

    void navigateToMemberDetailActivity(long id);
}
