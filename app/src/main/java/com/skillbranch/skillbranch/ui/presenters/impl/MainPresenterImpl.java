package com.skillbranch.skillbranch.ui.presenters.impl;

import com.skillbranch.skillbranch.R;
import com.skillbranch.skillbranch.ui.presenters.MainPresenter;
import com.skillbranch.skillbranch.ui.views.MainView;

/**
 * @author Sergey Vorobyev.
 */

public class MainPresenterImpl implements MainPresenter {

    private MainView view;

    public MainPresenterImpl(MainView view) {
        this.view = view;
    }

    public void onNavigationItemSelected(int itemId) {
        if (itemId == R.id.nav_item_1) {
            view.showFragmentInTabs1();
        } else if (itemId == R.id.nav_item_2) {
            view.showFragmentInTabs2();
        } else if (itemId == R.id.nav_item_3) {
            view.showFragmentInTabs3();
        }
        view.closeDrawer();
    }

    public void onBackPressed(boolean isOpenedDrawer) {
        if (isOpenedDrawer) {
            view.closeDrawer();
        } else {
            view.pressBack();
        }
    }
}
