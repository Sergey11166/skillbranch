package com.skillbranch.skillbranch.ui.presenters;

/**
 * @author Sergey Vorobyev
 */

public interface SplashPresenter {

    void loadData();
}
