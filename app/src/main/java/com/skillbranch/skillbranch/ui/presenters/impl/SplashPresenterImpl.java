package com.skillbranch.skillbranch.ui.presenters.impl;

import android.os.Handler;
import android.os.Looper;

import com.skillbranch.skillbranch.data.db.entities.AliasEntity;
import com.skillbranch.skillbranch.data.db.entities.DaoSession;
import com.skillbranch.skillbranch.data.db.entities.MemberEntity;
import com.skillbranch.skillbranch.data.db.entities.MemberEntityDao;
import com.skillbranch.skillbranch.data.db.entities.TitleEntity;
import com.skillbranch.skillbranch.data.network.RestService;
import com.skillbranch.skillbranch.data.network.models.HouseResponse;
import com.skillbranch.skillbranch.data.network.models.MemberResponse;
import com.skillbranch.skillbranch.ui.presenters.SplashPresenter;
import com.skillbranch.skillbranch.ui.views.SplashView;
import com.skillbranch.skillbranch.utils.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import rx.Observable;
import rx.schedulers.Schedulers;

import static com.skillbranch.skillbranch.data.db.entities.EntityUtils.cutIdFromUrl;
import static com.skillbranch.skillbranch.utils.Constants.HOUSE_LANNISTER_ID;
import static com.skillbranch.skillbranch.utils.Constants.HOUSE_STARK_ID;
import static com.skillbranch.skillbranch.utils.Constants.HOUSE_TARGARYEN_ID;
import static com.skillbranch.skillbranch.utils.UIUtils.handleResponseError;

/**
 * @author Sergey Vorobyev
 */

public class SplashPresenterImpl implements SplashPresenter {

    private static final String TAG = Constants.LOG_TAG_PREFIX + "SplashPresenter";

    private SplashView mView;
    private RestService mRestService;
    private DaoSession mDaoSession;
    private Handler mHandler;
    private List<MemberEntity> mMemberEntityList;
    private List<TitleEntity> mTitleEntityList;
    private List<AliasEntity> mAliasEntityList;

    private static final List<String> houseIds = Arrays
            .asList(HOUSE_STARK_ID, HOUSE_LANNISTER_ID, HOUSE_TARGARYEN_ID);

    public SplashPresenterImpl(SplashView view, RestService restService, DaoSession daoSession) {
        mView = view;
        this.mRestService = restService;
        this.mDaoSession = daoSession;
        this.mHandler = new Handler();
        this.mMemberEntityList = new ArrayList<>();
        this.mTitleEntityList = new ArrayList<>();
        this.mAliasEntityList = new ArrayList<>();
    }

    @Override
    public void loadData() {
        mView.showProgress();
        if (mDaoSession.getMemberEntityDao().count() != 0) {
            mHandler.postDelayed(this::onFinishedLoadData, 3000);
        } else {
            startLoad();
        }
    }

    private Observable<HouseResponse> createHousesObservable() {
        List<Observable<HouseResponse>> result = new ArrayList<>(3);
        for (String houseId: houseIds) result.add(mRestService.getHouse(houseId));
        return Observable.concat(result);
    }

    private Observable<MemberResponse> createMemberObservables(HouseResponse hr) {
        List<Observable<MemberResponse>> result = new ArrayList<>(hr.getSwornMembers().size());
        for (String url: hr.getSwornMembers()) result.add(mRestService.getMember(cutIdFromUrl(url)));
        return Observable.concat(result);
    }

    private void startLoad() {
        createHousesObservable()
                .subscribeOn(Schedulers.io())
                .subscribe(this::onNextHouse, this::handleError, this::onFinishedLoadData);
    }

    private void onNextHouse(HouseResponse hr) {
        createMemberObservables(hr)
                .subscribeOn(Schedulers.immediate())
                .subscribe(mr -> onNextMember(mr, hr), this::handleError);
    }

    private void onNextMember(MemberResponse mr, HouseResponse hr) {
        MemberEntity memberEntity = new MemberEntity(mr, cutIdFromUrl(hr.getUrl()), hr.getWords());
        mMemberEntityList.add(memberEntity);

        List<TitleEntity> titles = new ArrayList<>(mr.getTitles().size());
        for (String title: mr.getTitles()) titles.add(new TitleEntity(title, memberEntity.getRemoteId()));
        mTitleEntityList.addAll(titles);

        List<AliasEntity> aliases = new ArrayList<>(mr.getAliases().size());
        for (String alias: mr.getAliases()) aliases.add(new AliasEntity(alias, memberEntity.getRemoteId()));
        mAliasEntityList.addAll(aliases);

        String fatherId = cutIdFromUrl(mr.getFather());
        if (!fatherId.isEmpty()) {
            MemberEntity existEntity = mDaoSession.queryBuilder(MemberEntity.class)
                    .where(MemberEntityDao.Properties.RemoteId.eq(fatherId))
                    .build()
                    .unique();
            if (existEntity == null) {
                mRestService.getMember(fatherId)
                        .subscribeOn(Schedulers.immediate())
                        .subscribe(father -> onNextMember(father, hr), this::handleError);
            }
        }

        String motherId = cutIdFromUrl(mr.getFather());
        if (!motherId.isEmpty()) {
            MemberEntity existEntity = mDaoSession.queryBuilder(MemberEntity.class)
                    .where(MemberEntityDao.Properties.RemoteId.eq(motherId))
                    .build()
                    .unique();
            if (existEntity == null) {
                mRestService.getMember(motherId)
                        .subscribeOn(Schedulers.immediate())
                        .subscribe(father -> onNextMember(father, hr), this::handleError);
            }
        }
    }

    private void handleError(Throwable error) {
        handleResponseError(mHandler, TAG, error);
        onFinishedLoadData();
    }

    private void onFinishedLoadData() {
        saveAllMembersTx();
        mHandler.post(() -> {
            mView.hideProgress();
            mView.navigateToMainActivity();
        });
    }

    private void saveAllMembersTx() {
        mDaoSession.getMemberEntityDao().insertOrReplaceInTx(mMemberEntityList);
        mDaoSession.getTitleEntityDao().insertOrReplaceInTx(mTitleEntityList);
        mDaoSession.getAliasEntityDao().insertOrReplaceInTx(mAliasEntityList);
    }
}
