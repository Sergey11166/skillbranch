package com.skillbranch.skillbranch.ui.views.fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.Window;

import com.skillbranch.skillbranch.R;

public abstract class DaggerBaseFragment<C> extends Fragment {

    private static final String IS_PROGRESS_SHOWING_KEY = "IS_PROGRESS_SHOWING_KEY";

    protected C component;

    private ProgressDialog mProgressDialog;
    private boolean mIsProgressShowing;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setupComponent();
        super.onCreate(savedInstanceState);
        initProgressDialog();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mIsProgressShowing = savedInstanceState.getBoolean(IS_PROGRESS_SHOWING_KEY);
        }
        if (mIsProgressShowing) showProgress();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(IS_PROGRESS_SHOWING_KEY, mIsProgressShowing);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        mProgressDialog.dismiss();
        super.onDestroy();
    }

    public C getComponent() {
        return component;
    }

    private void initProgressDialog() {
        mProgressDialog = new ProgressDialog(getActivity(), R.style.custom_progress);
        mProgressDialog.setCancelable(false);
        Window window = mProgressDialog.getWindow();
        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    public void showProgress() {
        mProgressDialog.show();
        mProgressDialog.setContentView(R.layout.progress_splash);
    }

    public void hideProgress() {
        if (mProgressDialog.isShowing()) mProgressDialog.hide();
        mIsProgressShowing = false;
    }

    protected abstract void setupComponent();
}