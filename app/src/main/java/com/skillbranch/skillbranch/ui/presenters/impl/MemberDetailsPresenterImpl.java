package com.skillbranch.skillbranch.ui.presenters.impl;

import com.skillbranch.skillbranch.App;
import com.skillbranch.skillbranch.data.db.entities.DaoSession;
import com.skillbranch.skillbranch.data.db.entities.MemberEntity;
import com.skillbranch.skillbranch.ui.presenters.MemberDetailsPresenter;
import com.skillbranch.skillbranch.ui.views.MemberDetailsView;

import static com.skillbranch.skillbranch.data.db.entities.EntityUtils.cutIdFromUrl;

/**
 * @author Sergey Vorobyev.
 */

public class MemberDetailsPresenterImpl implements MemberDetailsPresenter {

    private DaoSession mDaoSession;
    private MemberDetailsView mView;
    private MemberEntity mOwner;
    private MemberEntity mFather;
    private MemberEntity mMother;

    public MemberDetailsPresenterImpl(MemberDetailsView view) {
        mView = view;
        mDaoSession = App.get().getComponent().daoSession();
    }

    @Override
    public void onFatherButtonClicked() {
        String url = mOwner.getFather();
        if (!url.isEmpty()) {
            mView.navigateToAnotherMember(Long.parseLong(cutIdFromUrl(url)));
        }
    }

    @Override
    public void onMotherButtonClicked() {
        String url = mOwner.getMother();
        if (!url.isEmpty()) {
            mView.navigateToAnotherMember(Long.parseLong(cutIdFromUrl(url)));
        }
    }

    @Override
    public void loadData(long id) {
        mOwner = mDaoSession.getMemberEntityDao().load(id);
        String fatherUrl = mOwner.getFather();
        if (!fatherUrl.isEmpty()) {
            mFather = mDaoSession.getMemberEntityDao().load(Long.valueOf(cutIdFromUrl(fatherUrl)));
        }
        String motherUrl = mOwner.getMother();
        if (!motherUrl.isEmpty()) {
            mMother = mDaoSession.getMemberEntityDao().load(Long.valueOf(cutIdFromUrl(motherUrl)));
        }
        showData();
    }

    private void showData() {
        mView.showData(mOwner, mFather, mMother);
        if (!mOwner.getDied().isEmpty()) {
            mView.showDied(mOwner.getDied());
        }
    }
}
