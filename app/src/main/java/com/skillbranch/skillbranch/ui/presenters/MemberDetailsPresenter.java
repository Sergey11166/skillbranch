package com.skillbranch.skillbranch.ui.presenters;

/**
 * @author Sergey Vorobyev.
 */

public interface MemberDetailsPresenter {

    void onFatherButtonClicked();
    void onMotherButtonClicked();
    void loadData(long id);
}
