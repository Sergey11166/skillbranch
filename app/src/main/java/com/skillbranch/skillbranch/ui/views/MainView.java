package com.skillbranch.skillbranch.ui.views;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * @author Sergey Vorobyev
 */

public interface MainView extends MvpView {

    void showFragmentInTabs1();
    void showFragmentInTabs2();
    void showFragmentInTabs3();
    void closeDrawer();
    void pressBack();
}
