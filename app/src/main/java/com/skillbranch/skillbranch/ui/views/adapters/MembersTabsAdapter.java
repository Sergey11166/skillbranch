package com.skillbranch.skillbranch.ui.views.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import static com.skillbranch.skillbranch.ui.views.fragments.MembersPageFragment.KEY_HOUSE_ID;

/**
 * @author Sergey Vorobyev.
 */

public class MembersTabsAdapter extends FragmentPagerAdapter {

    private final List<Fragment> mFragments = new ArrayList<>(3);
    private final List<String> mFragmentTitles = new ArrayList<>(3);

    public MembersTabsAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void addFragment(Fragment fragment, String houseId, String title) {
        Bundle args = new Bundle();
        args.putString(KEY_HOUSE_ID, houseId);
        fragment.setArguments(args);
        mFragments.add(fragment);
        mFragmentTitles.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitles.get(position);
    }
}