package com.skillbranch.skillbranch.ui.views.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skillbranch.skillbranch.R;
import com.skillbranch.skillbranch.di.components.MembersTabsComponent;
import com.skillbranch.skillbranch.di.modules.MembersTabsModule;
import com.skillbranch.skillbranch.ui.presenters.MembersTabsPresenter;
import com.skillbranch.skillbranch.ui.views.MembersTabsView;
import com.skillbranch.skillbranch.ui.views.activities.MainActivity;
import com.skillbranch.skillbranch.ui.views.adapters.MembersTabsAdapter;
import com.skillbranch.skillbranch.ui.views.events.ClickItemNavigationMenuEvent;
import com.skillbranch.skillbranch.utils.Constants;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.skillbranch.skillbranch.utils.Constants.HOUSE_LANNISTER_ID;
import static com.skillbranch.skillbranch.utils.Constants.HOUSE_STARK_ID;
import static com.skillbranch.skillbranch.utils.Constants.HOUSE_TARGARYEN_ID;

/**
 * @author Sergey Vorobyev.
 */

public class MembersTabsFragment extends DaggerBaseFragment<MembersTabsComponent> implements MembersTabsView {

    public static final String FRAGMENT_TAG = "HousesTabsFragment_FRAGMENT_TAG";

    @Inject
    MembersTabsPresenter presenter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.view_pager) ViewPager viewPager;
    @BindView(R.id.tabs) TabLayout tabLayout;

    private Unbinder unbinder;

    @Override
    protected void setupComponent() {
        super.component = ((MainActivity)getActivity()).getComponent()
                .plus(new MembersTabsModule(this));
        super.component.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_members, container, false);
        unbinder = ButterKnife.bind(this, view);
        viewPager.setOffscreenPageLimit(3);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        DrawerLayout drawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        setupDrawer(activity, drawerLayout);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void setupDrawer(Activity activity, DrawerLayout drawerLayout) {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(activity, drawerLayout, toolbar,
                R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setupViewPager() {
        MembersTabsAdapter adapter = new MembersTabsAdapter(getChildFragmentManager());
        adapter.addFragment(new MembersPageFragment(), HOUSE_STARK_ID, getString(R.string.house_item1));
        adapter.addFragment(new MembersPageFragment(), HOUSE_LANNISTER_ID, getString(R.string.house_item2));
        adapter.addFragment(new MembersPageFragment(), HOUSE_TARGARYEN_ID, getString(R.string.house_item3));
        viewPager.setAdapter(adapter);
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onNavigationMenuItemClick(ClickItemNavigationMenuEvent event) {
        switch (event.getHouseId()) {
            case Constants.HOUSE_STARK_ID:
                viewPager.setCurrentItem(0);
                break;
            case Constants.HOUSE_LANNISTER_ID:
                viewPager.setCurrentItem(1);
                break;
            case Constants.HOUSE_TARGARYEN_ID:
                viewPager.setCurrentItem(2);
                break;
        }
    }
}
