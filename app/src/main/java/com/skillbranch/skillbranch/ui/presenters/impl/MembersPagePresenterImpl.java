package com.skillbranch.skillbranch.ui.presenters.impl;

import android.os.Handler;
import android.util.Log;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.skillbranch.skillbranch.data.db.entities.DaoSession;
import com.skillbranch.skillbranch.data.db.entities.MemberEntity;
import com.skillbranch.skillbranch.ui.presenters.MembersPagePresenter;
import com.skillbranch.skillbranch.ui.views.MembersPageView;
import com.skillbranch.skillbranch.utils.Constants;

import org.greenrobot.greendao.async.AsyncSession;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

/**
 * @author Sergey Vorobyev.
 */

public class MembersPagePresenterImpl extends MvpBasePresenter<MembersPageView>
        implements MembersPagePresenter {

    private static final String TAG = Constants.LOG_TAG_PREFIX + "MembersPresenter";

    private DaoSession mDaoSession;
    private String houseId;
    private Handler mHandler;

    private List<MemberEntity> mData;

    public MembersPagePresenterImpl(String houseId, DaoSession daoSession) {
        this.houseId = houseId;
        this.mDaoSession = daoSession;
        this.mHandler = new Handler();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void loadNews(boolean pullToRefresh) {
        assert getView() != null;
        getView().showLoading(pullToRefresh);
        if (mData == null) {
            AsyncSession session = mDaoSession.startAsyncSession();
            session.setListener(operation -> {
                mData = (List<MemberEntity>) operation.getResult();
                filterResult(pullToRefresh);
            });
            session.loadAll(MemberEntity.class);
        } else {
            filterResult(pullToRefresh);
        }
    }

    private void filterResult(boolean pullToRefresh) {
        assert getView() != null;
        mHandler.postDelayed(() -> Observable.from(mData)
                .observeOn(AndroidSchedulers.mainThread())
                .filter(memberEntity -> memberEntity.getRemoteHouseId().equals(houseId))
                .toList()
                .subscribe(members -> {
                            getView().setData(members);
                            getView().showContent();
                        },
                        error -> getView().showError(error, pullToRefresh)
                ), 2000);
    }

    @Override
    public void onItemClick(long id) {
        assert getView() != null;
        getView().navigateToMemberDetailActivity(id);
    }

    @Override
    public void attachView(MembersPageView view) {
        super.attachView(view);
        Log.d(TAG, "attachView(" + view.toString() + ")");
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        Log.d(TAG, "detachView(" + retainInstance + ")");
    }
}
