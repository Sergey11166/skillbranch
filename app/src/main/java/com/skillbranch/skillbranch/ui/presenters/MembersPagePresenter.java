package com.skillbranch.skillbranch.ui.presenters;

import com.hannesdorfmann.mosby.mvp.MvpPresenter;
import com.skillbranch.skillbranch.ui.views.MembersPageView;

/**
 * @author Sergey Vorobyev
 */

public interface MembersPagePresenter extends MvpPresenter<MembersPageView> {

    void loadNews(boolean pullToRefresh);

    void onItemClick(long id);
}
