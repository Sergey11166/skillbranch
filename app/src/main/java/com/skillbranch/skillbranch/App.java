package com.skillbranch.skillbranch;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.skillbranch.skillbranch.di.components.AppComponent;
import com.skillbranch.skillbranch.di.components.DaggerAppComponent;
import com.skillbranch.skillbranch.di.modules.AppModule;

/**
 * @author Sergey Vorobyev
 */

public class App extends Application {

    private AppComponent mComponent;

    private static App app;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = createComponent();
        app = this;
        Stetho.initializeWithDefaults(this);
    }

    public AppComponent createComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule())
                .build();
    }

    public AppComponent getComponent() {
        return mComponent;
    }

    public static App get() {
        return app;
    }
}
