package com.skillbranch.skillbranch.di.modules;

import com.skillbranch.skillbranch.di.anotations.PerActivity;
import com.skillbranch.skillbranch.ui.presenters.MainPresenter;
import com.skillbranch.skillbranch.ui.presenters.impl.MainPresenterImpl;
import com.skillbranch.skillbranch.ui.views.MainView;
import com.skillbranch.skillbranch.ui.views.activities.MainActivity;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev.
 */

@Module
public class MainModule {

    private MainActivity activity;

    public MainModule(MainActivity activity) {
        this.activity = activity;
    }

    @Provides
    @PerActivity
    MainView provideMainView() {
        return activity;
    }

    @Provides
    @PerActivity
    MainPresenter provideMainPresenter(MainView view) {
        return new MainPresenterImpl(view);
    }
}
