package com.skillbranch.skillbranch.di.components;

import com.skillbranch.skillbranch.di.anotations.PerActivity;
import com.skillbranch.skillbranch.di.modules.MembersTabsModule;
import com.skillbranch.skillbranch.di.modules.MainModule;
import com.skillbranch.skillbranch.di.modules.MembersPageModule;
import com.skillbranch.skillbranch.ui.views.activities.MainActivity;

import dagger.Component;

/**
 * @author Sergey Vorobyev.
 */

@PerActivity
@Component(modules = MainModule.class, dependencies = AppComponent.class)
public interface MainComponent {

    void inject(MainActivity activity);

    MembersTabsComponent plus(MembersTabsModule module);
    MembersPageComponent plus(MembersPageModule module);
}
