package com.skillbranch.skillbranch.di.components;

import android.content.SharedPreferences;

import com.skillbranch.skillbranch.data.db.entities.DaoSession;
import com.skillbranch.skillbranch.data.network.RestService;
import com.skillbranch.skillbranch.di.modules.AppModule;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author Sergey Vorobyev
 */

@Singleton
@Component(modules = AppModule.class)

public interface AppComponent {

    SharedPreferences sharedPreferences();
    RestService restService();
    DaoSession daoSession();
    Picasso picasso();
}
