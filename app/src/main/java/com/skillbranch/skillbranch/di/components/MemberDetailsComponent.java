package com.skillbranch.skillbranch.di.components;

import com.skillbranch.skillbranch.di.anotations.PerActivity;
import com.skillbranch.skillbranch.di.modules.MemberDetailsModule;
import com.skillbranch.skillbranch.ui.views.activities.MemberDetailsActivity;

import dagger.Component;

/**
 * @author Sergey Vorobyev.
 */

@PerActivity
@Component(modules = MemberDetailsModule.class, dependencies = AppComponent.class)
public interface MemberDetailsComponent {

    void inject(MemberDetailsActivity activity);
}
