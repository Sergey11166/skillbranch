package com.skillbranch.skillbranch.di.modules;

import com.skillbranch.skillbranch.di.anotations.PerActivity;
import com.skillbranch.skillbranch.ui.presenters.MemberDetailsPresenter;
import com.skillbranch.skillbranch.ui.presenters.impl.MemberDetailsPresenterImpl;
import com.skillbranch.skillbranch.ui.views.MemberDetailsView;
import com.skillbranch.skillbranch.ui.views.activities.MemberDetailsActivity;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev.
 */

@Module
public class MemberDetailsModule {

    private MemberDetailsActivity mActivity;

    public MemberDetailsModule(MemberDetailsActivity activity) {
        mActivity = activity;
    }

    @Provides
    @PerActivity
    MemberDetailsView provideView() {
        return mActivity;
    }

    @Provides
    @PerActivity
    MemberDetailsPresenter providePresenter(MemberDetailsView view) {
        return new MemberDetailsPresenterImpl(view);
    }
}
