package com.skillbranch.skillbranch.di.modules;

import com.skillbranch.skillbranch.data.db.entities.DaoSession;
import com.skillbranch.skillbranch.data.network.RestService;
import com.skillbranch.skillbranch.di.anotations.PerFragment;
import com.skillbranch.skillbranch.ui.presenters.SplashPresenter;
import com.skillbranch.skillbranch.ui.presenters.impl.SplashPresenterImpl;
import com.skillbranch.skillbranch.ui.views.SplashView;
import com.skillbranch.skillbranch.ui.views.activities.SplashActivity;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev.
 */

@Module
public class SplashModule {

    private SplashActivity mActivity;

    public SplashModule(SplashActivity activity) {
        mActivity = activity;
    }

    @Provides
    @PerFragment
    SplashView provideView() {
        return mActivity;
    }

    @Provides
    @PerFragment
    SplashPresenter providePresenter(SplashView view, RestService restService, DaoSession daoSession) {
        return new SplashPresenterImpl(view, restService, daoSession);
    }
}
