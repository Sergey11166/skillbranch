package com.skillbranch.skillbranch.di.components;

import com.skillbranch.skillbranch.di.anotations.PerFragment;
import com.skillbranch.skillbranch.di.modules.MembersPageModule;
import com.skillbranch.skillbranch.ui.presenters.MembersPagePresenter;
import com.skillbranch.skillbranch.ui.views.fragments.MembersPageFragment;

import dagger.Subcomponent;

/**
 * @author Sergey Vorobyev.
 */


@PerFragment
@Subcomponent(modules = MembersPageModule.class)
public interface MembersPageComponent {

    void inject(MembersPageFragment fragment);

    MembersPagePresenter presenter();
}
