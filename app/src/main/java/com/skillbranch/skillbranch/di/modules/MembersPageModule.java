package com.skillbranch.skillbranch.di.modules;

import com.skillbranch.skillbranch.data.db.entities.DaoSession;
import com.skillbranch.skillbranch.di.anotations.PerFragment;
import com.skillbranch.skillbranch.ui.presenters.MembersPagePresenter;
import com.skillbranch.skillbranch.ui.presenters.impl.MembersPagePresenterImpl;
import com.skillbranch.skillbranch.ui.views.adapters.MembersPageAdapter;
import com.skillbranch.skillbranch.ui.views.fragments.MembersPageFragment;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev.
 */

@Module
public class MembersPageModule {

    private MembersPageFragment fragment;

    public MembersPageModule(MembersPageFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    MembersPagePresenter providePresenter(DaoSession daoSession) {
        String houseId = fragment.getArguments().getString(MembersPageFragment.KEY_HOUSE_ID);
        assert houseId != null;
        return new MembersPagePresenterImpl(houseId, daoSession);
    }

    @Provides
    @PerFragment
    MembersPageAdapter provideAdapter(Picasso picasso) {
        return new MembersPageAdapter(picasso);
    }
}
