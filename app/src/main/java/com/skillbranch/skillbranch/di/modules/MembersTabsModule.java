package com.skillbranch.skillbranch.di.modules;

import com.skillbranch.skillbranch.di.anotations.PerFragment;
import com.skillbranch.skillbranch.ui.presenters.MembersTabsPresenter;
import com.skillbranch.skillbranch.ui.presenters.impl.MembersTabsPresenterImpl;
import com.skillbranch.skillbranch.ui.views.MembersTabsView;
import com.skillbranch.skillbranch.ui.views.fragments.MembersTabsFragment;

import dagger.Module;
import dagger.Provides;

/**
 * @author Sergey Vorobyev.
 */

@Module
public class MembersTabsModule {

    private MembersTabsFragment fragment;

    public MembersTabsModule(MembersTabsFragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    @PerFragment
    MembersTabsView provideView() {
        return fragment;
    }

    @Provides
    @PerFragment
    MembersTabsPresenter providePresenter(MembersTabsView view) {
        return new MembersTabsPresenterImpl(view);
    }
}
