package com.skillbranch.skillbranch.di.components;

import com.skillbranch.skillbranch.di.anotations.PerFragment;
import com.skillbranch.skillbranch.di.modules.SplashModule;
import com.skillbranch.skillbranch.ui.views.activities.SplashActivity;

import dagger.Component;

/**
 * @author Sergey Vorobyev.
 */

@PerFragment
@Component(modules = SplashModule.class, dependencies = AppComponent.class)
public interface SplashComponent {

    void inject(SplashActivity activity);
}
