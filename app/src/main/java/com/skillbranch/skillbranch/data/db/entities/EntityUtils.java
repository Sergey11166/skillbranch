package com.skillbranch.skillbranch.data.db.entities;

/**
 * @author Sergey Vorobyev.
 */

public class EntityUtils {

    public static String cutIdFromUrl(String url) {
        String[] s = url.split("/");
        return s[s.length - 1];
    }
}
