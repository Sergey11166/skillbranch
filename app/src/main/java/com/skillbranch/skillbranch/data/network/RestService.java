package com.skillbranch.skillbranch.data.network;

import com.skillbranch.skillbranch.data.network.models.HouseResponse;
import com.skillbranch.skillbranch.data.network.models.MemberResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * @author Sergey Vorobyev.
 */

public interface RestService {

    @GET("houses/{houseId}")
    Observable<HouseResponse> getHouse(@Path("houseId") String houseId);

    @GET("characters/{memberId}")
    Observable<MemberResponse> getMember(@Path("memberId") String memberId);
}
